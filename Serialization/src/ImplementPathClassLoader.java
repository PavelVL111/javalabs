import interfaces.task8.PathClassLoader;

import java.io.*;

public class ImplementPathClassLoader extends ClassLoader implements PathClassLoader {
    private final String classPath[];
    private String path = null;

    ImplementPathClassLoader(String classPath[]){
        this.classPath = classPath;
    }

    protected synchronized Class loadClass(String name, boolean resolve) throws ClassNotFoundException{
        Class result = findClass(name);
        if (resolve){
            resolveClass(result);}
        return result;
    }

    protected Class findClass(String name) throws ClassNotFoundException {
        File file = findFile(name + ".class");
        Class result = null;

        if (file == null) {
            return findSystemClass(name);
        }
        try {
            byte classBytes[] = loadFileAsBytes(file);
            result = defineClass(name,classBytes,0,classBytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return result;
    }

    private File findFile(String name) {
        File file;
        file = new File(getPath(), name);
        if (file.exists()) {
            return file;
        }

        for (int i = 0; i < classPath.length; i++){
            file = new File(classPath[i], name);
            if (file.exists()){
                return file;}
        }
        return  null;
    }

    public static byte[] loadFileAsBytes(File file) throws IOException {
        byte byteFile[] = new  byte[(int)file.length()];
        try(FileInputStream fileInputStream = new FileInputStream(file)) {
            fileInputStream.read(byteFile, 0, byteFile.length);
        } catch (IOException e){
            e.printStackTrace();
        }
        return byteFile;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String getPath() {
        return path;
    }

}
