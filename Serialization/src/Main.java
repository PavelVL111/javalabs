import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, FileNotFoundException {
        ImplementCyclicCollection implementCyclicCollection = new ImplementCyclicCollection();

        ImplementCyclicItem implementCyclicItem1 = new ImplementCyclicItem();
        ImplementCyclicItem implementCyclicItem2 = new ImplementCyclicItem();
        ImplementCyclicItem implementCyclicItem3 = new ImplementCyclicItem();
        ImplementCyclicItem implementCyclicItem4 = new ImplementCyclicItem();

        implementCyclicItem1.setValue(1);
        implementCyclicItem2.setValue(2);
        implementCyclicItem3.setValue(3);
        implementCyclicItem4.setValue(4);

        implementCyclicItem1.setTemp(true);
        implementCyclicItem2.setTemp(true);
        implementCyclicItem3.setTemp(true);
        implementCyclicItem4.setTemp(true);

        implementCyclicCollection.add(implementCyclicItem1);
        implementCyclicCollection.add(implementCyclicItem2);
        implementCyclicCollection.add(implementCyclicItem3);

        implementCyclicCollection.insertAfter(implementCyclicItem3,implementCyclicItem4);
        implementCyclicCollection.remove(implementCyclicItem4);
        implementCyclicCollection.remove(implementCyclicItem2);


        FileOutputStream fos = new FileOutputStream("E:/temp.txt");

        ImplementSerializableUtils implementSerializableUtils = new ImplementSerializableUtils();
        implementSerializableUtils.serialize(fos,implementCyclicCollection);

        FileInputStream fis = new FileInputStream("E:/temp.txt");
        ImplementCyclicCollection iCCSerializable = (ImplementCyclicCollection) implementSerializableUtils.deserialize(fis);

         // Разные объекты из одного байт кода
        ImplementCyclicCollection ICCollection = new ImplementCyclicCollection();

        FileOutputStream fOStreem = new FileOutputStream("E:/temp.txt");

        ImplementSerializableUtils iSUtils = new ImplementSerializableUtils();
        iSUtils.serialize(fOStreem,ICCollection);

        FileInputStream fIStreemOne = new FileInputStream("E:/temp.txt");
        FileInputStream fIStreemTwo = new FileInputStream("E:/temp.txt");
        ImplementCyclicCollection iCCSerializableOne = (ImplementCyclicCollection) iSUtils.deserialize(fIStreemOne);
        ImplementCyclicCollection iCCSerializableTwo = (ImplementCyclicCollection) iSUtils.deserialize(fIStreemTwo);

        System.out.print(iCCSerializableOne.equals(iCCSerializableTwo));

        // Загрузчик

        ImplementPathClassLoader implementPathClassLoader = new
                ImplementPathClassLoader(new String[] {"E:\\dir\\classes\\class1","E:\\dir\\classes\\class2","E:\\dir\\classes\\class3"});
        Class loadClass = Class.forName("testClassOne", true, implementPathClassLoader);
        Object objectOne = loadClass.newInstance();
        System.out.println(objectOne);


        Class loadClassTwo = Class.forName("testClassTwo", true, implementPathClassLoader);
        Object objectTwo = loadClassTwo.newInstance();
        System.out.println(objectTwo);


        implementPathClassLoader.setPath("E:\\dir\\classes\\otherDir");
        Class loadClassOther = Class.forName("testClassOther", true, implementPathClassLoader);
        Object objectOthet = loadClassOther.newInstance();
        System.out.println(objectOthet);

        Class loadClassThree = Class.forName("testClassThree", true, implementPathClassLoader);
        Object objectThree = loadClassThree.newInstance();
        System.out.println(objectThree);

    }
}
