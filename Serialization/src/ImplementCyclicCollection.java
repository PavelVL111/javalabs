import interfaces.task8.CyclicCollection;
import interfaces.task8.CyclicItem;

import java.io.Serializable;


public class ImplementCyclicCollection implements CyclicCollection, Serializable {
    private CyclicItem first;
    private CyclicItem last;
    private int size;

    @Override
    public boolean add(CyclicItem item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (first == null) {
            first = item;
            first.setNextItem(first);
            last = item;
        } else {
            if (contains(item)) {
                throw new IllegalArgumentException();
            } else {
                last.setNextItem(item);
                last = item;
                last.setNextItem(first);

            }
        }
        size++;
        return true;
    }

    @Override
    public void insertAfter(CyclicItem item, CyclicItem newItem) {
        if (item == null || newItem == null) {
            throw new NullPointerException();
        }
        if (contains(newItem) || !contains(item)){throw new IllegalArgumentException();}
        newItem.setNextItem(item.nextItem());
        item.setNextItem(newItem);
        if (last.equals(item)) {
            last = last.nextItem();
        }
        size++;
    }

    @Override
    public CyclicItem getFirst() {
        return this.first;
    }

    @Override
    public boolean remove(CyclicItem item) {
        if (item == null) {
            throw new NullPointerException();
        }
        CyclicItem current = first;
        CyclicItem beforeItem = last;
        CyclicItem afterItem = current.nextItem();
        int i = size;
        if (size == 1) {
            first = null;
            last = null;
            item.setNextItem(null);
            return true;
        }
        while (0 != i--) {
            if (item.equals(current)) {
                if (item.equals(first)) {
                        first = afterItem;
                    }
                    if (item.equals(last)) {
                        last = beforeItem;
                    }
                    beforeItem.setNextItem(afterItem);
                    current.setNextItem(null);
                    size--;
                    return true;
            }
            current = current.nextItem();
            beforeItem = beforeItem.nextItem();
            afterItem = afterItem.nextItem();
        }

        return false;
    }


    @Override
    public int size() {
        return size;
    }

    private boolean contains(CyclicItem current) {
        int i = size;
        CyclicItem next = first;
        while (0 != i--) {
            if (current.equals((Object) next)) {
                return true;
            }
            next = next.nextItem();
        }
        return false;
    }

}

