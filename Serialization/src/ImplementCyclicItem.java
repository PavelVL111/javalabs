import interfaces.task8.CyclicItem;

import java.io.Serializable;


public class ImplementCyclicItem implements CyclicItem, Serializable {

    private Object value;
    private transient Object temp;
    private CyclicItem nextItem;

    @Override
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public void setTemp(Object temp) {
        this.temp = true;
    }

    @Override
    public Object getTemp() {
        return this.temp;
    }

    @Override
    public CyclicItem nextItem() {
        return nextItem;
    }

    @Override
    public void setNextItem(CyclicItem cyclicItem) {
        nextItem = cyclicItem;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object){ return true;}
        if (!(object instanceof ImplementCyclicItem)){ return false;}

        ImplementCyclicItem that = (ImplementCyclicItem) object;

        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
