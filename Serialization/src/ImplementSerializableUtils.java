import interfaces.task8.SerializableUtils;

import java.io.*;


public class ImplementSerializableUtils implements SerializableUtils{
    @Override
    public void serialize(OutputStream fos, Object implementCyclicCollection) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(implementCyclicCollection);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object deserialize(InputStream fis) {
        ImplementCyclicCollection iccSerializable = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(fis);
            iccSerializable = (ImplementCyclicCollection) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return iccSerializable;
    }
}
